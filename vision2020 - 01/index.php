<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>VISION 20/20 IN 2020 :: Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>
<div class="top">
        <img src="img/top-banner.png" width="100%" alt=""/> 
</div>
<div class="container-fluid">
  <div class="row mt-2 mb-4">
        <div class="col-12 col-md-6 offset-md-1 text-center">
            <div class="embed-responsive embed-responsive-16by9">
              <iframe class="embed-responsive-item" src="video.php" allowfullscreen></iframe>
            </div>
      </div>
        <div class="col-12 col-md-4">
            <div class="login">
                <div class="login-form">
                    <form id="question-form" method="post" role="form">
                      <div class="row">
                        <div class="col-10">
                            <h6>Ask your Question:</h6>
                            <div class="form-group">
                                <input class="form-control" type="text" id="user_name" name="user_name" placeholder="Enter Name" required>
                            </div>
                            <div class="form-group">
                               <textarea class="form-control" name="userQuestion" id="userQuestion" required placeholder="Please ask your question" rows="4"></textarea>
                            </div>
                        </div>
                      </div>
                      <div class="row mt-3">
                        <div class="col-10">
                            <input type="submit" class="btn btn-outline-success" value="Submit Question" alt="Submit">
                            <div id="message"></div>
                        </div>
                      </div>  
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(function(){

	$(document).on('submit', '#question-form', function()
    {  
            $.post('submitques.php', $(this).serialize(), function(data)
            {
                if(data=="success")
                {
                  $('#message').removeClass('alert-danger fail');
                  $('#message').addClass('alert-success success'); 
                  $('#message').text('Your question is submitted successfully.').fadeIn().delay(2000).fadeOut();
                  $('#question-form').find("textarea").val('');
                }
                else 
                {
                  $('#message').addClass('alert-danger fail');
                  $('#message').removeClass('alert-success success'); 
                  $('#message').text(data);
                }
                
            });
        
      
      return false;
    });
});
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-15"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-15');
</script>

</body>
</html>